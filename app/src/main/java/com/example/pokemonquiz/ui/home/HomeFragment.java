package com.example.pokemonquiz.ui.home;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.pokemonquiz.R;
import com.example.pokemonquiz.databinding.FragmentHomeBinding;
import android.widget.RadioGroup;
import android.widget.Toast;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private RadioGroup radioGroup;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

     /*   final TextView textView = binding.textHome;
        homeViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);*/
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        radioGroup = view.findViewById(R.id.question_1_radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radioButton_1) {
                    Toast.makeText(view.getContext().getApplicationContext(), Html.fromHtml("<font color='#ff0000' ><b>" + "Try again!" + "</b></font>"), Toast.LENGTH_LONG).show();
                }
                if (i == R.id.radioButton_2) {
                    Toast.makeText(view.getContext().getApplicationContext(), Html.fromHtml("<font color='#00ff00' ><b>" + "Success" + "</b></font>"), Toast.LENGTH_LONG).show();
                }
                if (i == R.id.radioButton_3) {
                    Toast.makeText(view.getContext().getApplicationContext(), Html.fromHtml("<font color='#ff0000' ><b>" + "Try again!" + "</b></font>"), Toast.LENGTH_LONG).show();
                }
            }
        });

        MediaPlayer mediaPlayer = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.pokemon_app_audio);
        mediaPlayer.start();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}